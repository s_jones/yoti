# core-interview-test

This is the submission by Simon Jones

## Client

The client implementing the `Client` interface can be found in `client_impl`

## Server

The server is available as a package located in `server`, and an executable in `server_main`.

It implements the beginings of a REST API. Storing is done by `POST`ing to `/storage/` with a body containing the following in `JSON` format:
```
{
    "Id":"<Base64 encoding of ID to store data under>",
    "Data":"<Base64 encoding of data to store>"
}
```
The response will be `JSON` of the form:
```
{
    "Key":"<Base64 encoding of the key used to encrypt the data>"
}
```

Retrieving is done by requesting a `GET` on the path `/storage/<id>?key=<base64key>`, where `id` is a URL escaped base64 encoding of the ID used to store the data under and `base64key` is the key returned by the `POST`, also URL escaped. The response will be `JSON` of the form:
```
{
    "Data":"<Base64 encoding of data stored>"
}
```
