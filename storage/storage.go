package storage

import (
    "crypto/aes"
    "crypto/rand"
)

type Id string
type Key [32]byte

type StorageError struct {
    what string
}

func (e StorageError) Error() string {
    return e.what
}

type Storage struct {
    data map[Id][]byte
}

func NewStorage() *Storage {
    return &Storage{make(map[Id][]byte)}
}

func (s *Storage) Store(id Id, data []byte) (Key, error) {
    key := Key{}
    _, err := rand.Read(key[:])
    if err != nil {
        return Key{}, StorageError{err.Error()}
    }

    cipher, err := aes.NewCipher(key[:])
    if err != nil {
        return Key{}, StorageError{err.Error()}
    }

    enc := data
    if last := len(data) % cipher.BlockSize(); last != 0 {
        enc = append(enc, make([]byte, cipher.BlockSize() - last)...)
        enc[len(enc) - 1] = byte(cipher.BlockSize() - last)
    } else {
        enc = append(enc, make([]byte, cipher.BlockSize())...)
        enc[len(enc) - 1] = byte(cipher.BlockSize())
    }

    cipher.Encrypt(enc, enc)

    s.data[id] = enc
    return key, nil
}

func (s *Storage) Retrieve(id Id, key Key) ([]byte, error) {
    data, ok := s.data[id]
    if !ok {
        return nil, StorageError{"No such entry"}
    }

    cipher, err := aes.NewCipher(key[:])
    if err != nil {
        return nil, StorageError{err.Error()}
    }

    cipher.Decrypt(data, data)

    padding := int(data[len(data) - 1])
    return data[:len(data) - padding], nil
}
