package storage

import (
    "bytes"
    "testing"
)

func TestStore(t *testing.T) {
    s := NewStorage()

    var id Id = "id1"
    data := []byte("Test")
    key, err := s.Store(id, data[:])
    if err != nil {
        t.Error("Key was nil")
    }

    var other_id Id = "id2"
    if k, _ := s.Store(other_id, data[:]); k == key {
        t.Error("Duplicate key")
    }

    if bytes.Equal(s.data[id], data) {
        t.Error("Stored data isn't encrypted")
    }
}

func TestRetrieve(t *testing.T) {
    s := NewStorage()

    var id Id
    out, err := s.Retrieve(id, Key{})
    if err == nil {
        t.Error("Expected error for invalid ID")
    }

    data := []byte("Test")
    key, err := s.Store(id, data)
    if err != nil {
        t.Fatalf("Failed to store data: %v", err.Error())
    }

    out, err = s.Retrieve(id, key)
    if err != nil {
        t.Fatalf("Failed to retrieve stored data: %v", err.Error())
    }
    if !bytes.Equal(out, data) {
        t.Errorf("Entry %v: expected '%v', got '%v'", id, data, out)
    }
}
