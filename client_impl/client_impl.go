package client_impl

import (
    "bitbucket.org/s_jones/yoti/client"
    "bytes"
    "encoding/base64"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/url"
)

type RetrieveError struct {
    code int
    what string
}

func (e RetrieveError) Error() string {
    return fmt.Sprintf("Got %v: %v", e.code, e.what)
}

type ClientImpl struct {
    addr string
}

func NewClientImpl(addr string) client.Client {
    return ClientImpl{addr}
}

func (c ClientImpl) Store(id, payload []byte) ([]byte, error) {
    type Req struct {
        Id []byte
        Data []byte
    }
    req :=  Req{id, payload}
    encoded, err := json.Marshal(req)
    if err != nil {
        return nil, err
    }

    resp, err := http.Post("http://" + c.addr + "/storage/", "application/json", bytes.NewReader(encoded))
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }

    type Ret struct {
        Key string
    }
    var ret Ret
    err = json.Unmarshal(body, &ret)
    if err != nil {
        return nil, err
    }

    key, err := base64.StdEncoding.DecodeString(ret.Key)
    if err != nil {
        return nil, err
    }

    return key, nil
}

func (c ClientImpl) Retrieve(id, aesKey []byte) ([]byte, error) {
    var req url.URL

    req.Scheme = "http"
    req.Host = c.addr

    enc_id := base64.StdEncoding.EncodeToString(id)
    req.Path = "/storage/" + url.QueryEscape(enc_id)

    query := req.Query()
    enc_key := base64.StdEncoding.EncodeToString(aesKey)
    query.Set("key", enc_key)
    req.RawQuery = query.Encode()

    resp, err := http.Get(req.String())
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        err = RetrieveError{resp.StatusCode, resp.Status}
        return nil, err
    }

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }

    type Ret struct {
        Data string
    }
    var ret Ret
    err = json.Unmarshal(body, &ret)
    if err != nil {
        return nil, err
    }

    data, err := base64.StdEncoding.DecodeString(ret.Data)
    if err != nil {
        return nil, err
    }

    return data, nil
}
