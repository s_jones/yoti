package client_impl

import (
    "bitbucket.org/s_jones/yoti/server"
    "bytes"
    "testing"
)

var s *server.Server = server.NewServer()

func TestStore(t *testing.T) {
    go s.Run()

    c := NewClientImpl("localhost:8080")

    id := [1]byte{1}
    data := []byte("Test")
    key, err := c.Store(id[:], data)
    if err != nil {
        t.Error("Key was nil")
    }

    other_id := [1]byte{2}
    if k, _ := c.Store(other_id[:], data[:]); bytes.Equal(k, key) {
        t.Error("Duplicate key")
    }
}

func TestRetrieve(t *testing.T) {
    go s.Run()

    c := NewClientImpl("localhost:8080")

    // id := [1]byte{1}
    id := []byte("Id1")
    out, err := c.Retrieve(id[:], id[:])
    if err == nil {
        t.Error("Expected error for invalid ID")
    }

    data := []byte("Test")
    key, err := c.Store(id[:], data)
    if err != nil {
        t.Fatalf("Failed to store data: %v", err.Error())
    }

    out, err = c.Retrieve(id[:], key)
    if err != nil {
        t.Fatalf("Failed to retrieve stored data: %v", err.Error())
    }
    if !bytes.Equal(out, data) {
        t.Errorf("Entry %v: expected '%v', got '%v'", id, data, out)
    }
}
