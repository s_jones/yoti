package server

import (
    "bitbucket.org/s_jones/yoti/storage"
    "encoding/base64"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/url"
    "strings"
)

type Server struct {
    Storage *storage.Storage
}

func NewServer() *Server {
    server := Server{storage.NewStorage()}
    http.HandleFunc("/storage/", server.Handle)
    return &server
}

func (s *Server) HandlePost(w http.ResponseWriter, r *http.Request) {
    body_raw, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Printf("ReadAll: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    type Info struct {
        Id string
        Data string
    }
    var info Info
    err = json.Unmarshal(body_raw, &info)
    if err != nil {
        fmt.Printf("Unmarshal: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    data, err := base64.StdEncoding.DecodeString(info.Data)
    if err != nil {
        fmt.Printf("DecodeString: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    key, err := s.Storage.Store(storage.Id(info.Id), data)
    if err != nil {
        fmt.Printf("Store: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    type Ret struct {
        Key string
    }
    var ret Ret
    ret.Key = base64.StdEncoding.EncodeToString(key[:])

    encoded, err := json.Marshal(ret)
    if err != nil {
        fmt.Printf("Marshal: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    _, err = w.Write(encoded)
    if err != nil {
        fmt.Printf("Write: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
}

func (s *Server) HandleGet(w http.ResponseWriter, r *http.Request) {
    query := r.URL.Query()
    _, ok := query["key"]
    if !ok {
        fmt.Println("No key")
        w.WriteHeader(http.StatusBadRequest)
        return
    }

    key_str, err := base64.StdEncoding.DecodeString(query["key"][0])
    if err != nil {
        fmt.Printf("DecodeString: %v", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
    var key storage.Key
    copy(key[:], []byte(key_str))

    paths := strings.SplitAfter(r.URL.Path, "/")
    if len(paths) != 3 {
        fmt.Println("No id", paths)
        w.WriteHeader(http.StatusBadRequest)
        return
    }
    id, err := url.QueryUnescape(paths[2])
    if err != nil {
        fmt.Printf("QueryUnescape: %v\n", err.Error())
        w.WriteHeader(http.StatusBadRequest)
        return
    }

    type Ret struct {
        Data []byte
    }
    var ret Ret
    ret.Data, err = s.Storage.Retrieve(storage.Id(id), storage.Key(key))
    if err != nil {
        fmt.Printf("Retrieve: %v\n", err.Error())
        w.WriteHeader(http.StatusBadRequest)
        return
    }

    encoded, err := json.Marshal(ret)
    if err != nil {
        fmt.Printf("Marshal: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }

    _, err = w.Write(encoded)
    if err != nil {
        fmt.Printf("Write: %v\n", err.Error())
        w.WriteHeader(http.StatusInternalServerError)
        return
    }
}

func (s *Server) Handle(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
        case http.MethodPost:
            s.HandlePost(w, r)
        case http.MethodGet:
            s.HandleGet(w, r)
        default:
            fmt.Printf("%v not supported\n", r.Method)
            w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

func (s *Server) Run() {
    http.ListenAndServe(":8080", nil)
}
