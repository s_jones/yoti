package main

import (
    "bitbucket.org/s_jones/yoti/server"
    "fmt"
)

func main() {
    server := server.NewServer()
    fmt.Println("Starting server…")
    server.Run()
}
